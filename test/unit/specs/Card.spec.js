import Vue from 'vue'
import Card from '@/components/Card'

describe('Card.vue', () => {
  const Constructor = Vue.extend(Card)
  const vm = new Constructor({
    propsData: {
      // Props are passed in "propsData".
      item: {
        title: 'test',
        main_image_url: 'http://google.com/image.jpg',
        link_url: 'http://www.google.com'
      }
    }
  }).$mount()

  it('should render correct title', () => {
    expect(vm.$el.querySelector('a h3').textContent)
    .to.equal('test')
  })

  it('should render correct link', () => {
    expect(vm.$el.getAttribute('href'))
    .to.equal('http://www.google.com')
  })

  it('should render correct image', () => {
    expect(vm.$el.querySelector('a div')
    .getAttribute('style'))
    .to.equal('background-image: url(http://google.com/image.jpg?w=350&h=240);')
  })
})
