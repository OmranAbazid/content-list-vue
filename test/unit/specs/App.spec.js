import VueResource from 'vue-resource'
import Vue from 'vue'
import App from '@/App'

Vue.use(VueResource)
Vue.http.interceptors.unshift((request, next) => {
  next(
    request.respondWith(
      {
        id: 1,
        content: [
          {
            title: 'testTitle',
            main_image_url: 'http://google.com/image.jpg',
            link_url: 'http://www.google.com'
          }
        ]
      },
      { status: 200 }
    )
  )
})

describe('App.vue', () => {
  it('should return correct data', () => {
    // vm.isError = true
    // vm.error = "404"
    const Constructor = Vue.extend(App)
    const vm = new Constructor()

    vm.$mount()
    vm.error = 'error'

    Vue.nextTick(() => {
      expect(vm.$el.querySelector('a').getAttribute('href')).to.equal(
        'http://www.google.com'
      )
      expect(vm.$el.querySelector('h3').textContent).to.equal('testTitle')
    })
  })

  it('should returnan error', () => {
    const Constructor = Vue.extend(App)
    const vm = new Constructor()

    vm.$mount()
    vm.error = 'error'

    Vue.nextTick(() => {
      expect(vm.$el.querySelector('h2').textContent).to.equal('error')
    })
  })
})
