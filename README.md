# Content-list-vue
A widget that lists the content items from Idio API

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run test

```

## Technology used
- Vue framework with HTML, CSS
- Vue resource is used with vue to issue Jsonp requests
- Testing is done using karma and mocha 
    -   Karma: test runner to be able to test html element using phantomjs
    -   Mocha: testing framework

## components
-   Card.vue: HTML, CSS, Javascript for a sigle Card
-   App.vue: the container component container multiple cards

## Reasons for choosing the technology
- Vue.js provide high seperation of concern between components
    -   each component is self contained
    -   for example, The card component can be taken from this app into
        another app easily
    - this is usefull if this project is part of a biger application
- vue props allow for writing less javascript code
    - the content list is populated directly in html without the need
    to create extra javscript code for that
- for the unit testing mocha and karma intergrate well with javascript
and more importantly they are easy to setup


## Possible improvements
- add test coverage 
- remove extra webpack configuration that is not need
    - this project uses vue-webpack template which has extra webpack configuration
    that is not needed in this project such as sass loader and other stuff